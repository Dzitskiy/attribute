<Query Kind="Program" />

void Main()
{
	var fruits=Food.apple|Food.grape;   //0b0101
	var vegetables=Food.potato|Food.cabbage;  //0b1010
	var mix=Food.potato|Food.cabbage|Food.grape;  //0b1110
	((fruits&Food.apple)==Food.apple).Dump();
	fruits.HasFlag(Food.apple).Dump();
	fruits.HasFlag((Food) 0).Dump(); 
	fruits.HasFlag(vegetables).Dump();
}

// You can define other methods, fields, classes and namespaces here

enum Food:short{
	apple    =  0b0001,
	potato   =  0b0010,
	grape    =  0b0100,
	cabbage  =  0b1000
}

//explanation
//0b0001
//0b0100

//0b0101
//0b0001

//0b0001