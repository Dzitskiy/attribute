﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Attributes
{
    [Serializable]
    class TestClass
    {
        private int _a;

        private bool B { get; set; }

        public void M1(int x)
        {
            Console.WriteLine($"x = {x}");
        }

        public void M2(int x = 15)
        {
            Console.WriteLine($"x = {x}");
        }

        public void M3(params int[] x)
        {
            Console.WriteLine($"x = {x.Length}");
        }

        [Obsolete("This method is obsolete. Use method New() instead.")]
        public void Obsolete() { }

        [Conditional("TEST")]
        public void C()
        {
            Console.WriteLine("Test passed");
        }
    }

    public static class SolverTask
    {
        public static int SolveProblem()
        {
            var numberOfClassAttributes = Extensions.GetSetBitCount((long)typeof(TestClass).GetTypeAttributes());
            numberOfClassAttributes+= typeof(TestClass).GetCustomAttributes(true).Length;    
            var numberOfMethodAttributes = 0;
            foreach ( var meth in typeof(TestClass).GetMethods()) 
            {
                numberOfMethodAttributes+= Extensions.GetSetBitCount((long)meth.Attributes);
                numberOfMethodAttributes += meth.GetCustomAttributes(true).Length;
            }


            //ToDo: finish solution in a same way with properties, fields and method parameters attribute for class

            return numberOfClassAttributes + numberOfMethodAttributes;

            
        }
        
    }

}
