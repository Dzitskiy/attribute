﻿using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Attributes;

//[StructLayout(LayoutKind.Explicit)]
//[StructLayout(LayoutKind.Auto)]
[StructLayout(LayoutKind.Sequential, Pack=2)]
struct Struct
{
    public byte a; // 1 byte
    public int b; // 4 bytes
    public short c; // 2 bytes
    public byte d; // 1 byte
}

[MyTest(AdditionalProperty ="Some information")]
public struct struct1
{
    public byte a; // 1 byte
    public int b; // 4 bytes
    public short c; // 2 bytes
    public byte d; // 1 byte
}


[StructLayout(LayoutKind.Sequential, Pack = 0)]
struct AdditionalExampleStruct
{
    public byte b1; //1 byte 
    public byte b2; //1 byte 
    public int i3;  //4 byte
}

public class LayoutInMemoryExample
{
    public unsafe static void Show()
    {

        AdditionalExampleStruct ex = new AdditionalExampleStruct();
        byte* addr = (byte*)&ex;
        Console.WriteLine("Size:      {0}", sizeof(AdditionalExampleStruct));
        Console.WriteLine("b1 Offset: {0}", &ex.b1 - addr);
        Console.WriteLine("b2 Offset: {0}", &ex.b2 - addr);
        Console.WriteLine("i3 Offset: {0}", (byte*)&ex.i3 - addr);
    }
}


[StructLayout(LayoutKind.Sequential, Pack = 0)]
struct PerfStructEx1
{
    public byte b1; //1 byte
    public int i1; //4 byte 
    public int i2; //4 byte     

    public PerfStructEx1()
    {
        b1=byte.MaxValue;
        i1 = int.MaxValue;
        i2 = int.MaxValue;
    }
}

[StructLayout(LayoutKind.Sequential, Pack = 0)]
struct PerfStructEx2
{
    public byte b1; //1 byte 
    public byte b2; //1 byte 
    public byte b3; //1 byte
    public byte b4; //1 byte
    public int i2; //4 byte      
    public PerfStructEx2()
    {
        b1 = byte.MaxValue;
        b2=byte.MaxValue;
        b3 = byte.MaxValue;
        b4 = byte.MaxValue;
        i2 = int.MaxValue;
    }
}

public static class PerfEx
{
    public static void ShowPerformanceExample_first()
    {
        Stopwatch sw = new Stopwatch();

        PerfStructEx1[] arr1 = new PerfStructEx1[100_000_000];

       
        sw.Start();
        for (int i = 0; i < 100_000_000; i++) { arr1[i] = new PerfStructEx1(); }
        sw.Stop();
        Console.WriteLine(sw.ElapsedMilliseconds);


    }

    public static void ShowPerformanceExampleSecond()
    {
        Stopwatch sw = new Stopwatch();


        PerfStructEx2[] arr2 = new PerfStructEx2[100_000_000];

        sw.Start();
      
        for (int i = 0; i < 100_000_000; i++) { arr2[i] = new PerfStructEx2(); }
        sw.Stop();
        Console.WriteLine(sw.ElapsedMilliseconds);

    }
}