﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Attributes;

public enum TypesOfProduct
{
    Vegetables=1,
    Fruits=2,
    Meat=4,
    Fish=8
}

public enum Cities
{
    Novgorod=1,
    Tver=2,
    Vladimir=4
}

public enum Shops
{

    shop1=1,
    [CitiAttribute(Cities.Novgorod)]
    [TypeOfProducts(TypesOfProduct.Vegetables|TypesOfProduct.Fruits)]
    shop2=2,
    [CitiAttribute(Cities.Tver)]
    [TypeOfProducts(TypesOfProduct.Fruits|TypesOfProduct.Meat)]
    shop3=3,
    shop4=4,
}

[AttributeUsage(AttributeTargets.Field)]
public class CitiAttribute : System.Attribute
{
    public Cities HomeCity { get; private set; }
    public CitiAttribute(Cities city)
    {
        HomeCity = city;
    }
    public override string ToString()
    {
        return HomeCity.ToString("F");
    }
}

[AttributeUsage(AttributeTargets.Field)]
public class TypeOfProductsAttribute : System.Attribute
{
    public TypesOfProduct Product { get; private set; }
    public TypeOfProductsAttribute(TypesOfProduct product)
    {
        Product = product;
    }
    public override string ToString()
    {
        return Product.ToString("F");
    }
}

public static class EnumExt
{
    public static string AboutShop(this Shops shop)
    {
        var shopType = shop.GetType();
        var shopName = Enum.GetName(shopType, shop);
        var homeCities = shopType.GetField(shopName).GetCustomAttributes(false).OfType<CitiAttribute>().Select(a => a.ToString()).Aggregate("", (a, b) => a + " " + b);
        var productsVariety = shopType.GetField(shopName).GetCustomAttributes(false).OfType<TypeOfProductsAttribute>().Select(a => a.ToString()).Aggregate("", (a, b) => a + " " + b);
        return $"{shop}" + (homeCities.ToString()!=""?$" home city - {homeCities.ToString()}":"")+ (productsVariety.ToString()!=""?$", could sell products : {productsVariety}":"");
    }
}



public static class CustomAttribForEnumExample
{
    public static void ShowExample() 
    {
        foreach(Shops a in Enum.GetValues(typeof(Shops)))
        {
            Console.WriteLine(a.AboutShop());
        }
        Console.WriteLine();
            
           
    }
}
