﻿namespace Attributes;

[AttributeUsage(AttributeTargets.Class|AttributeTargets.Struct)]
public class MyTestAttribute : System.Attribute
{
    public String Name { get; set; }
    public MyTestAttribute(string testAttribute = "")
    {
        Name = this.GetType().Name;
    }

    public String AdditionalProperty { get; set; } = "";

    public override string ToString()
    {
        return $"{Name} - {AdditionalProperty}";
    }
}

public static class GetValueOfCustomAttribExtension
{
    public static string GetValueOfTestAttribute(this Object o)
    {
        var attribute =(MyTestAttribute) System.Attribute.GetCustomAttribute(o.GetType(), typeof(MyTestAttribute));
        return attribute==null?"Not found": attribute.ToString();
    }
}